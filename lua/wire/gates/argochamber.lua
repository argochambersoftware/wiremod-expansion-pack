--[[
	Argochamber Extra gates
]]

local compile = function(code)
	--Output lua code.
	local lua = code;
	
	--!! REPLACE IMPORT STATEMENTS !!--
	lua = lua:gsub("::([%w%s%-_/%.]*);", "require(\"%1\");");
	
	local ALLCHAR = "[%w%d%s%+%-%*/]+";
	
	--!! REPLACE BACKSTICKS STATEMENTS !!--
	lua = lua:gsub("``", "repeat ");
	lua = lua:gsub("`(%w*)%s*=("..ALLCHAR.."),("..ALLCHAR.."),("..ALLCHAR..")`", "for %1=%2, %3, %4 do ");
	lua = lua:gsub("`(%w*)%s*=("..ALLCHAR.."),("..ALLCHAR..")`", "for %1=%2, %3 do ");
	lua = lua:gsub("`("..ALLCHAR..")`", "for _=1, %1 do ");
	lua = lua:gsub("`%?%s*("..ALLCHAR..")`", "while %1 do ");
	lua = lua:gsub("`%!%s*("..ALLCHAR..")`", "until %1");
	
	--!! REPLACE SPECIAL OPERATORS OF x= KIND !!--
	lua = lua:gsub("(%w*%s*)([%+%-%*/])(=)(%s*%d+)", "%1= %1%2%4");
	
	--!! REPLACE OPERATORS OF ++ & -- TYPES !!--
	lua = lua:gsub("(%w+%s*)%+%+;?", "%1 = %1+1;");
	lua = lua:gsub("(%w+%s*)%-%-;?", "%1 = %1-1;");
	
	--!! REPLACE LOCAL VARIABLES !!--
	lua = lua:gsub("%%%a+", "local %1"):gsub("%%", "");
	
	--!! FUNCTIONS !!--
	lua = lua:gsub("@", "return ");
	lua = lua:gsub(":([^:])", "function %1");
	
	--!! IF THEN ELSE AND BLOCKS !!--
	lua = lua:gsub("!", "end");
	lua = lua:gsub("%?%?(.-)\n", "if%1 then\n");
	lua = lua:gsub("%?%s*\n", "else\n");
	lua = lua:gsub("%?(.-)\n", "elseif%1 then\n");
	
	--!! POINTERS !!--
	lua = lua:gsub("->", ":");
	
	--!! REPLACE BLOCK COMMENTS !!--
	lua = lua:gsub("/%*%*(.-)%*/", "--[[---------------------%1--]]---------------------");
	lua = lua:gsub("/%*(.-)%*/", "--[[%1]]");
	
	--!! REPLACE SINGLE-LINE COMMENTS !!--
	lua = lua:gsub("//", "--");
	
	--Finally return the code.
	return lua;
end

GateActions("Argochamber")

-- JK Flip Flop
GateActions["argo_flipflop"] = {
	name = "JK Flip Flop",
	inputs = { "clk" },
	inputtypes = { "NORMAL" },
	outputs = { "A", "B" },
	outputtypes = { "NORMAL", "NORMAL" },
	output = function(gate, clk)
		local tick = ( clk > 0 );
		if (gate.jk == nil) then gate.jk = false; end
		if (tick == true) then
			gate.jk = not gate.jk;
		end;
		if (gate.jk == true) then
			return 0, 1;
		else
			return 1, 0;
		end
	end,
	label = function(Out, clk)
		local fp = "Flip";
		if (Out.A == 0) then fp = "Flop"; end
		return string.format ("{ %s }", fp);
	end
}

GateActions["argo_sqrt"] = {
	name = "Square Root (With imaginary)",
	inputs = { "A" },
	inputtypes = { "NORMAL" },
	outputs = { "Real", "Imaginary" },
	outputtypes = { "NORMAL", "NORMAL" },
	output = function(gate, A)
		--We do need to compute this.
		if (A < 0) then
			return 0, math.sqrt(math.abs(A));
		else
			return math.sqrt(A), 0;
		end
	end,
	label = function(Out, A)
		return string.format("sqrt(%s) = %d + %di", A, Out.Real, Out.Imaginary);
	end
}

GateActions["argo_squarejk"] = {
	name = "Square JK FlipFlop",
	inputs = { "Run", "Reset", "PulseTime" },
	outputs = { "A", "B" },
	outputtypes = { "NORMAL", "NORMAL" },
	timed = true,
	output = function(gate, Run, Reset, PulseTime )
		local DeltaTime = CurTime()-(gate.PrevTime or CurTime())
		gate.PrevTime = (gate.PrevTime or CurTime())+DeltaTime
	
		if (gate.jk == nil) then gate.jk = false; end
		
		if (Reset > 0) then
			gate.Accum = 0
		elseif (Run > 0) then
			gate.Accum = gate.Accum+DeltaTime
			if (gate.Accum >= PulseTime ) then
				gate.Accum = 0
				gate.jk = not gate.jk;
			end
		end
		
		if (gate.jk == true) then
			return 0, 1;
		else
			return 1, 0;
		end
		
	end,
	reset = function(gate)
		gate.PrevTime = CurTime()
		gate.Accum = 0
	end,
	label = function(Out, Run, Reset, PulseTime, GapTime)
		local fp = "Flip";
		if (Out.A == 0) then fp = "Flop"; end
		return string.format ("{ %s }", fp);
	end
}

GateActions["pcc_gate"] = {
	name = "Papaya Compiler",
	inputs = { "Code" },
	inputtypes = { "STRING" },
	outputtypes = { "STRING" },
	output = function(gate, Code)
		if !Code then Code = "//No code provided" end
		return compile(Code);
	end,
	label = function(Out, Code)
		return string.format ("Code >> PCC >> Out");
	end
}

GateActions["chamber_buffer"] = {
	name = "Buffer Gate",
	inputs = { "Enable", "A", "B", "C", "D", "E", "F", "G", "H" },
	inputtypes = { "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL" },
	outputs = { "A", "B", "C", "D", "E", "F", "G", "H" },
	outputtypes = { "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL" },
	compact_inputs = 2,
	output = function(gate, Enable, A, B, C, D, E, F, G, H)
		if (Enable > 0) then
			return A, B, C, D, E, F, G, H;
		else
			return 0, 0, 0, 0, 0, 0, 0, 0;
		end
	end,
	label = function(Out, Enable)
		return string.format("Enable[%s] = { %i, %i, %i, %i, %i, %i, %i, %i }",
			Enable, Out.A, Out.B, Out.C, Out.D, Out.E, Out.F, Out.G, Out.H );
	end
}

GateActions()
