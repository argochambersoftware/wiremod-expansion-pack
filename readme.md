# Wirechamber
#### Argochamber Interactive 2016

*Wirechamber* is a [Wiremod's](https://github.com/wiremod/wire) expansion pack for **Argochamber's Servers**.

You can use it in your own server, but do not redistribute or steal, give proper credits.

## Workshop Installation

At the moment there's no planned _workshop addon creation_ for this, because we do not certainly know if
the wiremod's team will allow us.

For now, clients do not need to download models so, only is needed at the **server**.

## Installation

Just clone **this** repository into your **addons** folder (You know, old style).